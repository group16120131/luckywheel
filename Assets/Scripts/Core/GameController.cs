using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : Singleton<GameController>
{
    public List<String> Names = new List<string>();
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        SetDataName(10);
    }
    
    public void SetDataName(int amount)
    {
        Names.Clear();
        for (int i = 0; i < amount; i++)
        {
            Names.Add($"Name {i}");
        }
    }
}