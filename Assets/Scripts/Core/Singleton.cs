using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T s;

    public static T S
    {
        get
        {
            if (s == null)
            {
                s = FindObjectOfType(typeof(T)) as T;

                if (s == null)
                {
                    s = new GameObject().AddComponent<T>();
                    s.gameObject.name = s.GetType().Name;
                }
            }
            return s;
        }
    }

    public void Reset()
    {
        s = null;
    }

    public static bool Exists()
    {
        return (s != null);
    }
}