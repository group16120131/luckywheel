using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constant
{
    public const string SCENE_Splash = "Splash";
    public const string SCENE_Game = "Game";

    public static Dictionary<int, string> NameColor = new Dictionary<int, string>()
    {
        {0, $"#FF0000" }, // Red
        {1, $"#00FF00" }, // Green
        {2, $"#0000FF" }, // Blue
        {3, $"#FFFF00" }, // Yellow
        {4, $"#00FFFF" }, // Cyan
    };
}
