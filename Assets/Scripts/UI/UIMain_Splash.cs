using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UIMain_Splash : MonoBehaviour
{
    [SerializeField] private GameObject m_PlayButton;

    public void OnClick_Play()
    {
        SceneManager.LoadScene(Constant.SCENE_Game);
    }
}
