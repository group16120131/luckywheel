using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMain_LuckyWheel : MonoBehaviour
{
    [Header("Main Panel")]
    [SerializeField] private LuckyWheelItem m_ItemPrefab;
    [SerializeField] private Transform m_MainWheel;
    [SerializeField] private GameObject m_BackButton;
    [SerializeField] private Spinner m_Spinner;

    [Header("Result Panel")]
    [SerializeField] private GameObject m_ResultPanel;
    [SerializeField] private TextMeshProUGUI m_NameResult;

    [Header("AddName Panel")]
    [SerializeField] private GameObject m_AddNamePanel;
    [SerializeField] private TMP_InputField m_InputName;

    private List<LuckyWheelItem> items = new List<LuckyWheelItem>();
    private int currentIndex = -1;
    private bool isSpinning;
    private int total;
    private void Start()
    {
        GenerateWheel();
    }

    private void GenerateWheel() 
    {
        ActiveResultPanel(false);
        ActiveResultPanel(false);
        m_MainWheel.DestroyChildren();
        total = GameController.S.Names.Count;
        for (int i = 0; i < total; i++)
        {
            var Item = Instantiate(m_ItemPrefab, m_MainWheel);
            Item.transform.localScale = new Vector3(1, 1, 1);
            var item = Item.GetComponent<LuckyWheelItem>();
            item.Init(i);
            items.Add(item);
        }
    }
    private bool CanSpin()
    {
        return currentIndex != -1;
    }
    public void OnClick_SpinButton()
    {
        if (isSpinning)
        {
            Debug.Log("It is spinning !");
            return;
        }
        if (total <= 0)
        {
            Debug.Log("Empty Choice !");
            return;
        }
        StartCoroutine(ISpin());
    }
    private IEnumerator ISpin()
    {
        isSpinning = true;
        m_BackButton.SetActive(false);
        currentIndex = Random.Range(0, total);
        Debug.Log("Result " + currentIndex);
        yield return new WaitUntil(CanSpin);
        m_Spinner.PlaySpin(currentIndex, total, () =>
        {
            isSpinning = false;
            Debug.Log(currentIndex);
            StartCoroutine(IShowResult(currentIndex));
        });
    }

    private IEnumerator IShowResult(int index)
    {
        yield return new WaitForSeconds(0.5f);
        m_BackButton.SetActive(true);
        ActiveResultPanel(true);
        m_NameResult.text = GameController.S.Names[index];
    }
    private void ActiveResultPanel(bool active)
    {
        m_ResultPanel.SetActive(active);
    }
    private void ActiveAddNamePanel(bool active)
    {
        m_AddNamePanel.SetActive(active);
    }
    public void OnClick_SubmitAddNameButton()
    {
        if (!string.IsNullOrEmpty(m_InputName.text))
        {
            char[] separators = { ',', ';', '\n', '-' }; 
            string[] names = m_InputName.text.Split(separators);
            var nameList = names.ToList();
            if(nameList.Count > 0)
            {
                GameController.S.Names.Clear();
                for(int i = 0; i < nameList.Count; i++)
                {
                    GameController.S.Names.Add(nameList[i]);
                }
            }
            GenerateWheel();
        }
        ActiveAddNamePanel(false);
    }
    public void OnClick_CancelAddNameButton()
    {
        ActiveAddNamePanel(false);
    }
    public void OnClick_OpenAddNameButton()
    {
        ActiveAddNamePanel(true);
    }
    public void OnClick_CloseResultPanelButton()
    {
        ActiveResultPanel(false);
        currentIndex = -1;
    }
    public void OnClick_DeleteNameResultPanelButton()
    {
        GameController.S.Names.RemoveAt(currentIndex);
        Debug.Log($"Delete at {currentIndex}");
        GenerateWheel();
        currentIndex = -1;
        ActiveResultPanel(false);
    }
    public void OnClick_BackButton()
    {
        if (isSpinning)
        {
            Debug.Log($"It is spinning !");
            return;
        }
        SceneManager.LoadScene(Constant.SCENE_Splash);
    }
    public void OnClick_OKButton()
    {
        if (isSpinning)
        {
            Debug.Log($"It is spinning !");
            return;
        }
        GameController.S.SetDataName(Random.Range(10, 20));
        GenerateWheel();
    }
}
