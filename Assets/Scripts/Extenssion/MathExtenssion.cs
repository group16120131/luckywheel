using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathExtenssion
{
    public static float ConvertDegreeToRadian(float degree)
    {
        return degree * Mathf.PI / 180f;
    }
    public static float ConvertRadianToDegree(float radian)
    {
        return radian * 180f / Mathf.PI;
    }
}
