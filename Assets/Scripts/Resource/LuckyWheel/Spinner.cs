using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    [Header("Spin Animation")]
    public Transform Spin;
    [SerializeField] float spinTime1 = 1.5f;
    [SerializeField] float spinTime2 = 0.75f;
    [SerializeField] float spinTime3 = 0.25f;
    public void PlaySpin(int rewardIndex, float total, UnityEngine.Events.UnityAction finish = null)
    {
        var itemCount = total;
        var totalAngle = 360f;
        var unitAngler = 360f / itemCount;
        var angle1 = totalAngle * UnityEngine.Random.Range(5,7);
        var angle2 = totalAngle * UnityEngine.Random.Range(1, 3);
        float targetAngle = -unitAngler * rewardIndex + 360f;
        Spin.DOLocalRotate(new Vector3(0, 0, angle1), spinTime1, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).OnComplete(() =>
        {
            Spin.DOLocalRotate(new Vector3(0, 0, angle2), spinTime2, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).OnComplete(() =>
            {
                Spin.DOLocalRotateQuaternion(Quaternion.Euler(0f, 0f, targetAngle), spinTime3).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        finish?.Invoke();
                    });
            });
        });
    }
}
