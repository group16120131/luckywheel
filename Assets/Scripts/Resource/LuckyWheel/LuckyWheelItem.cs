using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LuckyWheelItem : MonoBehaviour
{
    [SerializeField] private Transform m_Content;
    [SerializeField] private Image m_Graphic;
    [SerializeField] private TextMeshProUGUI m_NameText;
    public int m_index;
    public int Index => m_index;
    public void Init(int id)
    {
        m_index = id;
        SetUp();
    }
    private void SetUp()
    {
        var totalItem = (float)GameController.S.Names.Count;
        var unitAngler = 360f / totalItem;
        var angler = -90f - unitAngler / 2f + unitAngler * m_index;
        m_Graphic.fillAmount = 1f/totalItem;
        Color color;
        if (ColorUtility.TryParseHtmlString(Constant.NameColor[m_index % Constant.NameColor.Count], out color))
        {
            m_Graphic.color = color;
        }
        
        transform.localEulerAngles = new Vector3(0f, 0f, angler);
        var posX = -200f * Mathf.Sin(MathExtenssion.ConvertDegreeToRadian(unitAngler) / 2f);
        var posY = 200f * Mathf.Cos(MathExtenssion.ConvertDegreeToRadian(unitAngler) / 2f);
        m_NameText.transform.localPosition = new Vector2(posX, posY);
        m_NameText.transform.localRotation = Quaternion.Euler(0f, 0f, 90f + unitAngler / 2f);
        m_NameText.text = GameController.S.Names[m_index];
    }
    
}
